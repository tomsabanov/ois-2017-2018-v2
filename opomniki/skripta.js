function onClickButton($){
	var name = document.querySelector('#uporabnisko_ime').value;
	var field = document.getElementById('uporabnik');
	
	field.innerHTML = name;
	
	var pokrivalo = document.querySelector('.pokrivalo');
	pokrivalo.style.visibility = 'hidden';

}

var id = 0;

function dodajOpomnik($){
	var naziv = document.getElementById('naziv_opomnika');
	var cas = document.getElementById('cas_opomnika');
	
	var naziv_val = naziv.value;
	var naziv_cas = cas.value;
	
	naziv.value = "";
	cas.value = "";
	
	
  var html= " \
    <div class='opomnik'> \
      <div class='naziv_opomnika  " + id + "' > " + naziv_val
 + " </div> \
      <div class='cas_opomnika'> Opomnik čez <span>" + naziv_cas
 + "</span> sekund. </div> \
    </div>";
	id++;
	document.getElementById("opomniki").innerHTML += html
	
	
}

window.addEventListener('load', function() {
	//stran nalozena


	var button = document.getElementById("prijavniGumb");
	
	button.addEventListener('click', onClickButton);

	var addButton  = document.getElementById('dodajGumb');
	addButton.addEventListener('click', dodajOpomnik);
	
	
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
	
		for (var i = 0; i <	 opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
				if(cas == 0){
					alert("Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!")
					document.querySelector("#opomniki").removeChild(opomnik);
				}
				else{
					cas--;
					casovnik.innerHTML = cas;
				}		//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);

});
